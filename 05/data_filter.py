import json

# Load data from JSON file
with open('data.json', 'r') as file:
    data = json.load(file)

# Extract the names, filter out those containing 'e', and sort them out
names = [item['name'] for item in data if 'name' in item and isinstance(item['name'], str)]
filtered_names = [name for name in names if 'e' not in name.lower()]
sorted_names = sorted(filtered_names)

# Write the sorted names to output.txt
with open('output.txt', 'w') as output_file:
    for name in sorted_names:
        output_file.write(name + '\n')

print("Names have been sorted, write to output file")
