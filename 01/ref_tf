# Centralized variables for ports
variable "ports" {
  type    = map(any)
  default = {
    mobile_envoy_gateway = [local.service.mobile_envoy_gateway.listen_port, 9901]
    admin_envoy_gateway  = [local.service.admin_envoy_gateway.listen_port, 9901]
    mobilegateway        = [local.service.stratus_mobilegateway_apfcu.listen_port, 9901]
    admingateway         = [local.service.stratus_admingateway_apfcu.listen_port, 9901]
    nucleus              = [local.service.stratus_nucleus_apfcu.listen_port, 9901]
    adapter              = [local.service.stratus_adapter_apfcu.listen_port, 9901]
    uno                  = [local.service.stratus_uno.listen_port, 9901]
  }
}

# Centralized list of security groups for egress rules
variable "egress_security_groups" {
  type    = list(string)
  default = [
    module.stratus_adapter_apfcu.security_group_id,
    module.stratus_admingateway.security_group_id,
    module.stratus_nucleus_apfcu.security_group_id,
    module.stratus_uno.security_group_id,
    module.stratus_mobilegateway.security_group_id
  ]
}

# Generic function to create ingress rules
resource "aws_vpc_security_group_ingress_rule" "generic_ingress" {
  for_each = {
    "mobile_envoy_gateway" : { security_group_id = module.mobile_envoy_gateway.security_group_id, reference = aws_security_group.mobile_alb.id }
    "admin_envoy_gateway"  : { security_group_id = module.admin_envoy_gateway.security_group_id, reference = aws_security_group.admin_alb.id }
    "mobilegateway"        : { security_group_id = module.stratus_mobilegateway.security_group_id, reference = module.mobile_envoy_gateway.security_group_id }
    "admingateway"         : { security_group_id = module.stratus_admingateway.security_group_id, reference = module.admin_envoy_gateway.security_group_id }
    "nucleus_mobile"       : { security_group_id = module.stratus_nucleus_apfcu.security_group_id, reference = module.stratus_mobilegateway.security_group_id }
    "nucleus_admin"        : { security_group_id = module.stratus_nucleus_apfcu.security_group_id, reference = module.stratus_admingateway.security_group_id }
    "adapter"              : { security_group_id = module.stratus_adapter_apfcu.security_group_id, reference = module.stratus_nucleus_apfcu.security_group_id }
    "uno"                  : { security_group_id = module.stratus_uno.security_group_id, reference = module.stratus_adapter_apfcu.security_group_id }
  }

  security_group_id            = each.value.security_group_id
  from_port                    = port
  to_port                      = port
  ip_protocol                  = "tcp"
  referenced_security_group_id = each.value.reference
  description                  = "Ingress rule for ${each.key}"

  dynamic "ingress_port" {
    for_each = var.ports[each.key]
    content {
      from_port = ingress_port.value
      to_port   = ingress_port.value
    }
  }
}

# Generic function to create egress rules
resource "aws_vpc_security_group_egress_rule" "generic_egress" {
  for_each = {
    "mobile_envoy_gateway" : module.mobile_envoy_gateway.security_group_id
    "admin_envoy_gateway"  : module.admin_envoy_gateway.security_group_id
  }

  security_group_id = each.value
  from_port         = 9901
  to_port           = 9901
  ip_protocol       = "tcp"
  description       = "Egress rule for ${each.key}"

  dynamic "egress_group" {
    for_each = var.egress_security_groups
    content {
      referenced_security_group_id = egress_group.value
    }
  }
}

# Specific egress rule for uno
resource "aws_vpc_security_group_egress_rule" "uno_ecs_task_egress" {
  security_group_id            = module.stratus_uno.security_group_id
  from_port                    = 5432
  to_port                      = 5432
  ip_protocol                  = "tcp"
  referenced_security_group_id = aws_security_group.uno_rds.id
  description                  = "Egress TO RDS Instance"
}
