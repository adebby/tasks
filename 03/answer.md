In the CI folder, the SAST yml file setup is for setting the security controls for the AWS login and pipeline

the job yml file is for configuration and writing the jobs and stages for validating, formatting, linting, planning, and deploying Terraform infrastructure while using the AWS login auth already setup using the SAST file

the tf var is basically used for configuring the variable for deployment

.gitlab-ci.yml is a pipeline that takes all this dependencies above and deploy into the dev, stg and prod environments

the makefile defines the job yml file( validating, formatting, linting, planning, and deploying Terraform infrastructure) and also where S3 bucket is used 